const API_URL = 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c&page=1'
const IMG_PATH = 'https://image.tmdb.org/t/p/w1280'
const SEARCH_API = 'https://api.themoviedb.org/3/search/movie?api_key=3fd2be6f0c70a2a598f084ddfb75487c&query="'
const GENRE_API = 'https://api.themoviedb.org/3/genre/movie/list?api_key=3fd2be6f0c70a2a598f084ddfb75487c&language=en-US'
const VIDEO_API = 'https://api.themoviedb.org/3/movie/{movie_id}/videos?api_key=3fd2be6f0c70a2a598f084ddfb75487c&language=en-US'

const main = document.getElementById('main')
const form = document.getElementById('form')
const search = document.getElementById('search')
const next = document.getElementById('nextButton')
const prev = document.getElementById('prevButton')
const pagenmbr = document.getElementById('pagenumber')

var page = 0;
var optionValue;
let searchTerm;

// Get initial movies
getMovies(API_URL)
getGenre(GENRE_API)

//Fetch Movies data
async function getMovies(url) {
    const res = await fetch(url)
    const data = await res.json()
    page = data.page

    showMovies(data.results, data.total_pages)
}

//Fetch genre data
async function getGenre(url){
    const res = await fetch(url)
    const data = await res.json()

    showGenres(data.genres)
}

//get movies function
function showMovies(movies, totPages) {
    main.innerHTML = ''

    movies.forEach((movie) => {
        const { id, title, poster_path, vote_average, overview, } = movie

        const movieEl = document.createElement('div')
        movieEl.classList.add('movie')

        movieEl.innerHTML = 
        `
            <img src="${IMG_PATH + poster_path}" alt="${title}">
            <div class="movie-info">
                <h3>${title}</h3>
                <span class="${getClassByRate(vote_average)}">${vote_average}</span>
            </div>
            <div class="overview">
                <h3>Overview</h3>
                    ${overview}
                <br/>
                <button class="videobtn" id="${id}"> Watch Trailer</button>
            </div>
        `
        main.appendChild(movieEl)

        document.getElementById(id).addEventListener('click', () => {
            
            openNav(movie);
        })
    })

    pagenmbr.innerHTML = ` ${page} / ${totPages} `
    page == 1 ? prev.style.display ='none' :  prev.style.display ='inline'
    page == totPages ? next.style.display ='none' : next.style.display ='inline'
}

//Get movie based on genre function
function MuvieGenre(){
    var e = document.getElementById("genre");
    optionValue = e.value;

    main.innerHTML ='';
    getMovies(API_URL + '&with_genres=' +optionValue)
}

//Get genre function
function showGenres(genres){

    genres.forEach((genre) => {
        const {id, name} = genre
        const genresList = document.createElement('option');
        const attr = document.createAttribute("value")

        attr.value = `${id}`;
        genresList.innerHTML = `${name}`;
        genresList.setAttributeNode(attr);

        document.getElementById('genre').appendChild(genresList);
    })
}

//Rating function
function getClassByRate(vote) {
    if(vote >= 8) {
        return 'green'
    } else if(vote >= 5) {
        return 'orange'
    } else {
        return 'red'
    }
}

//Button function
function prevPage(){
    page--
    updateMovies(page);
}
function nextPage(){   
    page++;
    updateMovies(page);
}

form.addEventListener('submit', (e) => {
    e.preventDefault()

    searchTerm = search.value

    if(searchTerm && searchTerm !== '') {
        getMovies(SEARCH_API + searchTerm)

        search.value = ''
    } else {
        window.location.reload()
    }
})

//Pagination function
function updateMovies(page){
    if(searchTerm && searchTerm !== ''){
        getMovies(SEARCH_API + searchTerm +'&page='+page)
    }
    else if(optionValue !== ''){
        getMovies('https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c&page='+page+'&with_genres='+optionValue )
    }
    else{
        getMovies('https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c&page='+page)
    }
    
}

const content = document.getElementById('content')
/* Open when someone clicks on the span element */
function openNav(movie) {
    var id = movie.id;

    fetch('https://api.themoviedb.org/3/movie/'+id+'/videos?api_key=3fd2be6f0c70a2a598f084ddfb75487c&language=en-US').then(res => res.json())
    .then(videoData => {
        if(videoData){

            document.getElementById("myNav").style.width = "100%";
            
            if(videoData.results.length > 0){
                
                var youtube = [];
                videoData.results.forEach(video => {

                    let {key, name, site} = video

                    if(site == "YouTube"){

                        youtube.push(`
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/${key}" title="${name}" frameborder="0" allow="accelerometer; 
                        autoplay; 
                        clipboard-write; 
                        encrypted-media; 
                        gyroscope; 
                        picture-in-picture" allowfullscreen>
                        </iframe>
                        `)
                    }
                })
                content.innerHTML = youtube.join(' ');

            }else{
                content.innerHTML = `<h1 style="color: white">No Video Found</h1>`
            }
        }

    })
    

}
  
/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}